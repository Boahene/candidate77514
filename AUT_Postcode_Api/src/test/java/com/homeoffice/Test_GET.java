package com.homeoffice;


import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.response.Response;

public class Test_GET {
	
	
	@Test
	public void Get_Request()
	{
		Response response = RestAssured.get("https://api.postcodes.io/postcodes/SW1P4JA");
		
		Assert.assertEquals(response.getStatusCode(), 200);
		
		System.out.println(response.getStatusCode());
		
	}

}
